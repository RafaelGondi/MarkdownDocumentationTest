<h1>Dialogs</h1>
<p>Dialog boxes for web and mobile usage. CLick <a href="https://rafaelgondi.gitlab.io/MarkdownDocumentationTest/">here</a> to see it in action</p>  


  
<a name="$"></a>

<a name="$"></a>

## $ : <code>object</code>
<strong>description of the $ object</strong>

**Kind**: global namespace  

* [$](#$) : <code>object</code>
    * [.dialog](#$.dialog) : <code>object</code>
        * [.this.waiting(m)](#$.dialog.this.waiting)
        * [.this.confirm(title, message, confirm, cancel)](#$.dialog.this.confirm)
        * [.this.prompt(title, m, label, selector, func)](#$.dialog.this.prompt)
        * [.this.info(title, m, func)](#$.dialog.this.info)

<a name="$.dialog"></a>

### $.dialog : <code>object</code>
The dialogs function have methods to create these dialog boxes: wait, confirm, prompt and info

**Kind**: static namespace of [<code>$</code>](#$)  

* [.dialog](#$.dialog) : <code>object</code>
    * [.this.waiting(m)](#$.dialog.this.waiting)
    * [.this.confirm(title, message, confirm, cancel)](#$.dialog.this.confirm)
    * [.this.prompt(title, m, label, selector, func)](#$.dialog.this.prompt)
    * [.this.info(title, m, func)](#$.dialog.this.info)

<a name="$.dialog.this.waiting"></a>

#### dialog.this.waiting(m)
This method creates a dialog box with just a message and no interaction.

**Kind**: static method of [<code>dialog</code>](#$.dialog)  

| Param | Type | Description |
| --- | --- | --- |
| m | <code>string</code> | [The message that will be displayed within the dialog box] |

**Example**  
```js
$.dialog.waiting('Please wait, Synchronizing Data...');
```
![](dialogWait.png)

<a name="$.dialog.this.confirm"></a>

#### dialog.this.confirm(title, message, confirm, cancel)
This method creates a dialog box with a title, a message and 'confirm' and 'cancel' options.

**Kind**: static method of [<code>dialog</code>](#$.dialog)  

| Param | Type | Description |
| --- | --- | --- |
| title | <code>string</code> | [The title of the dialog box] |
| message | <code>string</code> | [The message that will be displayed on the body of the dialog box] |
| confirm | <code>function</code> | [The function that will be called if the user selects the 'confirm' option] |
| cancel | <code>function</code> | [The function that will be called if the user selects the 'cancel' option] |

**Example**  
```js
$.dialog.confirm("Confirmação", "Tem certeza que deseja sair? Os dados não salvos serão descartados.",
									function() {
	 											$.dialog.close();
												window.history.back();
												}
											);
```
![](dialogConfirm.png)
<a name="$.dialog.this.prompt"></a>

#### dialog.this.prompt(title, m, label, selector, func)
This method creates a prompt box with a title, a message, a label, a selector and one function.

**Kind**: static method of [<code>dialog</code>](#$.dialog)  

| Param | Type | Description |
| --- | --- | --- |
| title | <code>string</code> | [The title of the dialog box] |
| m | <code>string</code> | [The message that will be displayed on the body of the dialog box] |
| label | <code>string</code> | [The label of the dialog] |
| selector | <code>string</code> | [String that will what the input field will select when the users presses 'OK'] |
| func | <code>function</code> | [The function that will be called if the user selects the 'OK' option] |

**Example**  
```js
$.dialog.prompt('Título do diálogo', 'Corpo do diálogo', 'Label do diálogo', 'teste',
									function() {
											alert('Callback ao clicar em ok');
										}
								);
```
![](dialogPrompt.png)
<a name="$.dialog.this.info"></a>

#### dialog.this.info(title, m, func)
This method creates a dialog box with a title, a message and one function.

**Kind**: static method of [<code>dialog</code>](#$.dialog)  

| Param | Type | Description |
| --- | --- | --- |
| title | <code>string</code> | [The title of the dialog box] |
| m | <code>string</code> | [The message that will be displayed on the body of the dialog box] |
| func | <code>string</code> | [The function that will be called if the user selects to close the modal] |

**Example**  
```js
$.dialog.info('Titulo do Diálogo', 'Corpo do Diálogo',
								function() {
									 			alert('Callback ao clicar em fechar');
									 		});
```									 		
![](dialogInfopng.png)